require 'token_redis_session_store'
require 'rack/response'
RSpec.describe ActionDispatch::Session::TokenRedisStore do
  let :options do
    {}
  end

  subject(:store) { ActionDispatch::Session::TokenRedisStore.new(nil, options) }
  let(:redis) do
    double('redis').tap do |o|
      allow(store).to receive(:redis).and_return(o)
    end
  end
  let(:given_sid) { 'randomstring' }

  describe '#find_session' do
    context 'no session id' do
      it 'should create new session' do
        sid, session = store.find_session({}, nil)
        expect(sid).not_to be_nil
        expect(session).to eq({})
      end
    end
    context 'no existing session' do
      before do
        allow(redis).to receive(:get).and_return(nil)
      end
      it 'should create a new session' do
        sid, session = store.find_session({}, 'randomstring')
        expect(sid).not_to eq('randomstring')
        expect(session).to eq({})
      end
    end
    context 'session exists' do
      before do
        allow(redis).to receive(:get).and_return({ 'value' => 'string'}.to_json)
      end
      it 'should return the session' do
        sid, session = store.find_session({}, given_sid)
        expect(sid).to eq(given_sid)
        expect(session).to eq('value' => 'string')
      end
    end
  end
  describe '#write_session' do
    context 'expires' do
      it 'should set expiring session' do
        expect(redis).to receive(:setex)
        sid = store.write_session({}, given_sid, {}, expire_after: 10)
        expect(sid).not_to be_nil
      end
    end
    context 'doesn\'t expire' do
      it 'should set session' do
        expect(redis).to receive(:set)
        sid = store.write_session({}, given_sid, {}, {})
        expect(sid).not_to be_nil
      end
    end
  end
  describe '#delete_session' do
    let(:req) { Rack::Response.new }
    after(:each) do
      store.delete_session(req, given_sid, {})
    end
    it 'should remove header' do
      expect(req).to receive(:delete_header).with('HTTP_SESSION_ID')
    end
    it 'should delete redis entry' do
      expect(redis).to receive(:del).with(given_sid)
    end
    it 'should reset session id' do
      expect(store).to receive(:generate_sid)
    end
  end
end
