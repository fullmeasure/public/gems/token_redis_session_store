require 'redis'

module ActionDispatch
  module Session
    class TokenRedisStore < ActionDispatch::Session::AbstractStore
      def initialize(app, options = {})
        redis_options = options[:redis] || {}
        @session_header = options[:session_header] || 'Session-Id'
        @redis = Redis.new(redis_options)
        options[:defer] = true
        super
      end

      # Get a session from the cache.
      def find_session(_req, sid)
        unless sid && (session = parse_session(sid))
          sid = generate_sid
          session = {}
        end
        [sid, session]
      end

      # Set a session in the cache.
      def write_session(_req, sid, session, options)
        unless session.nil?
          if options[:expire_after].nil?
            redis.set(sid, session.to_json)
          else
            redis.setex(sid, options[:expire_after], session.to_json)
          end
        end
        sid
      end

      # Remove a session from the cache.
      def delete_session(req, sid, _session)
        req.delete_header "HTTP_#{@session_header.sub('-', '_').upcase}"
        redis.del(sid)
        generate_sid
      end

      private

      attr_reader :redis

      def extract_session_id(req)
        req.get_header "HTTP_#{@session_header.sub('-', '_').upcase}"
      end

      def parse_session(sid)
        (entry = redis.get(sid)) ? JSON.parse(entry) : nil
      end
    end
  end
end
